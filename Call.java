//Call Class
//Assignment One
//@Matt Hoing
//@version 1.0
 
 public class Call
{
    //The Attributes
    private String callId;
    private int numMinutes;
    private String callStatus;
    private boolean callBackCustomer;
    private String callAnsweredBy;
    private int customerSatisfactionRating;
    private static int suggestedLengthOfCall;
    
    //The Methods
    
    //The Constructor
    public Call(String id, int mins, String stat, boolean callBack, String ans, int custSat)
    {
        callId = id;
        numMinutes = mins;
        callStatus = stat;
        callBackCustomer = callBack;
        callAnsweredBy = ans;
        customerSatisfactionRating = custSat;
    }
    
    
    // method to return callId attribute
    public String getCallId()
    {
        return callId;
    }
    
    // method to return numMinutes attribute
    public int numMinutes()
    {
        return numMinutes;
    }
    
    // method to return callStatus attribute
    public String callStatus()
    {
        return callStatus;
    }
    
    // method to return callBackCustomer attribute
    public boolean callBackCustomer()
    {
        return callBackCustomer;
    }
    
    // method to return callAnsweredBy attribute
    public String callAnsweredBy()
    {
        return callAnsweredBy;
    }
    
    // method to return customerSatisfactionRating attribute
    public int customerSatisfactionRating()
    {
        return customerSatisfactionRating;
    }
    
    // method to return suggestedLengthOfCall attribute
    public static int suggestedLengthOfCall()
    {
        return suggestedLengthOfCall;
    }
    
    // method to set callId method
    public void setCallId(String id)
    {
        callId = id;
    }
        
    // method to set numMinutes attribute
    public void setNumMinutes(int mins)
    {
        numMinutes = mins;
    }
    
    // method to set callStatus method
    public void setCallStatus(String stat)
    {
        callStatus = stat;
    }
    
    // method to set callBackCustomer attribute
    public void setCallBackCustomer(boolean callBack)
    {
        callBackCustomer = callBack;
    }
    
    // method to set callAnsweredBy attribute
    public void setCallAnsweredBy(String ans)
    {
        callAnsweredBy = ans;
    }
    
    // method to set customerSatisfactionRating attribute
    public void setCustomerSatisfactionRating(int custSat)
    {
        customerSatisfactionRating = custSat;
    }
    
    // method to set suggestedLengthOfCall attribute
    public static void setSuggestedLengthOfCall(int callLength)
    {
        suggestedLengthOfCall = callLength;
    }
    //toString Method
    public String toString()
    {
        String temp;
        temp = "ID " + callId + "\nMinutes " + numMinutes + "\nCall Back " + callBackCustomer + "\nAnswered By " + callAnsweredBy + "\nCustomer Satisfaction Rating " + customerSatisfactionRating + "\nLength of call " + suggestedLengthOfCall;
        return temp;
    }
        
    }


    