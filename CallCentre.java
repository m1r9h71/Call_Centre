/**
 * Class Call Centre
 * @Matt Hoing
 * @v1.0
 */

public class CallCentre
{
    //attributes
    
    private int total;
    private Call[] allCalls;
    
   
    //methods
    //method to take the max number calls
    public CallCentre(int totalIn)
    {
        allCalls = new Call[totalIn];
        total=0;
    }
    // return total
    public int getTotal()
    {
        return total;
    }
    // add another call
    public boolean add(Call CallIn)
    {
        if (!isFull())
        {
            allCalls[total] = CallIn;
            total++;
            return true;
        }
        else
        {
            return false;
        }
    }
    //remove call from array
    public boolean remove(String CallIn)
    {
        int index = search(CallIn);
        if(index == 999)
        {
            return false;
        }
        else
        {
            for(int i = index; i<= total-2; i++)
            {
                allCalls[i] = allCalls[i+1];
            }
            total--;
            return true;
        }
    }
    //check array is empty
    public boolean isEmpty()
    {
        if(total==0){
            return true;}
            else{
                return false;}
    }
    //check array is full
    public boolean isFull()
    {
        if(total==allCalls.length){
            return true;}
            else{
                return false;
            }
                
    }
    //list all call variables
    public String list()
   {
       String list ="";
       if(!isEmpty()){
           for(int i = 0; i < total; i++){
               list += allCalls[i].getCallId()+ allCalls[i].numMinutes() + allCalls[i].callStatus() + allCalls[i].callBackCustomer() + allCalls[i].callAnsweredBy() + allCalls[i].customerSatisfactionRating();
            }
            return list;
        } 
        else{
            return "The list is empty";
        }
   }
    //check longest call
   public Call checkLongestCall()//check for call of longest duration
    {   
        int longest = 0; 
        int x = 0;
        for (int i=0; i<total; i++)
        {
        Call temCall = allCalls[i];
        int mins = temCall.numMinutes();
        if (mins > longest)
        {
            longest = mins;
            i = x;
        }
    }
    return allCalls[x];
}
public Call checkShortestCall()//method to check shortest call in array of calls
{
     int shortest = 0; 
     int x = 0;   
     for (int i = 0; i < total; i++)
     {
        Call temCall = allCalls[i];
        int min = temCall.numMinutes();
        if(min<shortest)
        {
            shortest = min;
            i=x;
        }
        }
        return allCalls[x];
    }
private int search(String callIn)//search for call
{
    for (int i = 0; i < total; i++)
    {
        Call temCall = allCalls[i];
        String tempId = temCall.getCallId();
        if (tempId.equals(callIn))
        {
            return i;
        }
    }
    return -999;
}
public String listCalls()//method to list all calls using toString method in call class
    {
        String strung="";
        for (int i=0; i< total; i++)
        {
            strung += allCalls[i].toString();
        }
        return strung;
    }
public Call getItem(String callIdIn)//search for call id 
    {
        int index;
        index = search(callIdIn);
        if(index == -999)
        {
            return null;
        }
        else
        {
            return allCalls[index];
        }
       
    }
public String listCallsAboveSuggestedLength() //list all calls above agreed length of time
    {
        int length;
        int i;
        length = Call.suggestedLengthOfCall();
        String strung = "";
        for (i = 0; i < total; i++)
        {
            Call temCall = allCalls[i];
            int mins = temCall.numMinutes();
            if(mins > length)
            {
                strung = allCalls[i].toString();
            }
        }
        return strung;
    }
    public void updateSuggestedLengthOfCall(int callLengthIn)//updates the suggested length of all calls
    {
        Call.setSuggestedLengthOfCall(callLengthIn);
    }
    
    
}
