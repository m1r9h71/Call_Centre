
/**
 * CallCentre Class
 * Assignment One
 * @Matt Hoing
 * @version 1.0
 */
import java.util.*;
public class CallCentreTester
{
    public static void main(String [] args)
    {
        Scanner sc = new Scanner(System.in);
        
        int choose;
        System.out.println("Please enter the maximum number of calls that can be taken in the centre: ");
        int max=EasyScanner.nextInt();
        CallCentre londonCalling = new CallCentre(max);
        System.out.println("");
        do
        {
            System.out.println("Call Centre System ");
            System.out.println("1. Add a call ");
            System.out.println("2. Remove a call ");
            System.out.println("3. Update call status ");
            System.out.println("4. Check if the call list is empty ");
            System.out.println("5. Check if the call list is full ");
            System.out.println("6. Call details..... ");
            System.out.println("7. Update suggested length of call ");
            System.out.println("8. Exit System ");
            System.out.println("");
            System.out.println("Please enter a choice [1-8 only]: ");
            
             choose = EasyScanner.nextInt();
            switch(choose)
            {
                case 1:
                addCall(londonCalling);
                break;
                
                case 2:
                removeCall(londonCalling);
                break;
                
                case 3:
                updateCall(londonCalling);
                break;
                
                case 4:
                callEmpty(londonCalling);
                break;
                
                case 5:
                callFull(londonCalling);
                break;
                
                case 6:
                callDetails(londonCalling);
                break;
                
                case 7:
                updateCallLength(londonCalling);
                break;
                
                case 8:
                System.out.println("Goodbye ");
                
                default:
                System.out.println("Please choose a valid option!");
            }
        }while(choose!=8);
    }
    public static void addCall(CallCentre londonCalling)
    {
         System.out.println("Enter call ID: ");
         String id = EasyScanner.nextString();
         System.out.print("Enter length of call: ");
         int mins = EasyScanner.nextInt();
         System.out.print("Enter call status [open, pending or closed]: ");
         String stat = EasyScanner.nextString();
         System.out.print("Does the customr need a call back [y/n]?");
         char callback = EasyScanner.nextChar();
         boolean callback2=true;
         if(callback =='y')
         {
            callback2 = true;
            }
            else if(callback=='n')
            {
                callback2 = false;
            }else
            {
                System.out.println("Invalid input! ");
            }
            
         System.out.print("Name of agent ");
         String ans = EasyScanner.nextString();
         System.out.print("Was the customer satisfied ");
         int custSat = EasyScanner.nextInt();
         Call callppp = new Call(id, mins, stat,callback2,ans,custSat );
         londonCalling.add(callppp);
    }     
    public static void removeCall(CallCentre londonCalling)
    {
        System.out.println("Enter a call ID: ");
        String id = EasyScanner.nextString();
    }
    public static void updateCall(CallCentre londonCalling)
    {
        System.out.println("Enter a call ID: ");
        String id = EasyScanner.nextString();
        System.out.println("Enter the status ");
        String status = EasyScanner.nextString();
        
    }
    public static void callEmpty(CallCentre londonCalling)
    {
        if (londonCalling.isEmpty())
        {
            System.out.println("The call lists are empty ");
        }
        else
        {
            System.out.println("The call lists are full ");
        }
    }
    public static void callFull(CallCentre londonCalling)
    {
        if (londonCalling.isFull())
        {
            System.out.println("The call lists are full ");
        }
        else
        {
            System.out.println("The call lists are empty ");
        }
    }
    public static void updateCallLength (CallCentre londonCalling)
    {
        System.out.println("Enter new call length ");
        int newCallLength = EasyScanner.nextInt();
        londonCalling.updateSuggestedLengthOfCall(newCallLength);
    }
    public static void callDetails(CallCentre londonCalling)
    {   
        char choose2;
        do{
        System.out.println("Call Centre System ");
        System.out.println("1. Add a call ");
        System.out.println("2. Remove a call ");
        System.out.println("3. Update call status ");
        System.out.println("4. Check if the call list is empty ");
        System.out.println("5. Check if the call list is full ");
        System.out.println("6. Call details..... ");
        System.out.println("7. Update suggested length of call ");
        System.out.println("8. Exit System ");
        System.out.println("");
        System.out.println("a. Display Details of a Specific Call: ");
        System.out.println("b. Display ALL Calls: ");
        System.out.println("c. Display longest call: ");
        System.out.println("d. Display shortest call: ");
        System.out.println("e. Display all calls above the suggested length of a call :");
        System.out.println("Enter choice [a-e]: ");
        
        choose2 = EasyScanner.nextChar();
        switch(choose2)
        {
        //display the details of a specific user entered call
        case 'a':
        System.out.println("Display details of a specific call: ");
        System.out.println("Please enter the Call ID: ");
        String lookUpId = EasyScanner.nextString();
        Call details = londonCalling.getItem(lookUpId);
        System.out.println(details.toString());
        break;
        //display list of all calls
        case 'b':
        if(!londonCalling.isEmpty())
        {
            System.out.println(londonCalling.listCalls());
            
        }
        else
        {
            System.out.println("No calls have been entered! ");
        }
        break;
        //Display longest call details
        case 'c':
        if(!londonCalling.isEmpty())
        {
            Call longest = londonCalling.checkLongestCall();
            System.out.println(longest.toString());
        }
        else
        {
            System.out.println("No calls have been added! ");
        }
        break;
        //display the shortest call details
        case 'd':
        if(!londonCalling.isEmpty())
        {
            Call shortest = londonCalling.checkShortestCall();
            System.out.println(shortest.toString());
        }
        else
        {
            System.out.println("No calls have been added! ");
        }
        break;
        //list all calls above the call length added by user in addCall() method
        case 'e':
        if(!londonCalling.isEmpty())
        {
            System.out.println(londonCalling.listCallsAboveSuggestedLength());
        }
        else
        {
            System.out.println("No calls have been added! ");
        }
        break;
        //if user enters anything other than a-e (rolls eyes)
        default:
        System.out.println("Entry invalid, Please enter a letter a, b, c, d or e ");
        
        }
    }while(choose2 !='a' || choose2 !='b' || choose2 !='c' || choose2 !='d' || choose2 !='e');
}
}

